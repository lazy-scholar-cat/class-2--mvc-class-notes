## 在Models中建立模型

在Models中创建表模型.cs文件


![无法加载](./imgs/2021-12-13_203412.png)


**Apartemnt.cs**

![无法加载](./imgs/2021-12-13_204340.png)

**Emploee.cs**

![无法加载](./imgs/2021-12-13_204431.png)

这两个文件是我建立的。


## 创建数据库上下文，配置数据库连接字符


```c#
dotnet add package Microsoft.EntityFrameworkCore -v 3.1
```

**XXX.csproj**文件中就会出现

```c#
  <ItemGroup>
    <PackageReference Include="Microsoft.EntityFrameworkCore" Version="3.1" />
  </ItemGroup>
```

说明成功了。


**创建Db文件夹建立.cs文件**


![无法加载](./imgs/2021-12-13_205142.png)


**.cs文件**

![无法加载](./imgs/2021-12-17_100907.png)

**提醒一下**关于连接数据库的字符串中,server并不是一直等于 . 的只是机房电脑的服务是 .，在自己的电脑要换成自己电脑的服务。不然会**用户 'sa' 登录失败。**有特殊符号要转义。

```
builder.UseSqlServer();报错
上面代码需要以下代码在XXX.csproj存在，写完要dotnet restore
<ItemGroup>
    <PackageReference Include="Microsoft.EntityFrameworkCore.SqlServer" Version="3.1" />
</ItemGroup>
```



```
.cs文件我没写public DbSet<xxx> xxx{set;get;}
可能是这个原因以至于后来的Migrations文件中的.cs文件没有东西。

后来问老胡了确实是这个原因。
```





DbSet：表示可用于创建，读取，更新和删除操作的实体集。



DbSet 常用的方法：
Add：将给定的实体添加到添加状态的上下文中。当保存更改时，添加状态中的实体将被插入到数据库中。保存更改后，对象状态将更改为“未更改”。


Remove：将给定的实体标记为已删除。保存更改后，实体将从数据库中删除。在调用此方法之前，实体必须存在于其他某个状态的上下文中。


## 生成数据库表


```
生成数据库表代码

dotnet ef migrations add InitialCreate

```

**出现这个报错需要下载一个tool**


```
PS C:\Users\Administrator\Desktop\juses> dotnet ef migratio
无法执行，因为找不到指定的命令或文件。
可能的原因包括:
  *你拼错了内置的 dotnet 命令。
  *你打算执行 .NET Core 程序，但 dotnet-ef 不存在。
  *你打算运行全局工具，但在路径上找不到名称前缀为 dotnet 的


  tool代码：
  dotnet tool install --global dotnet-ef --version 3.1.1-*
```

成功

![无法加载](./imgs/2021-12-13_211745.png)

### Microsoft.EntityFrameworkCore.Design报错
![无法加载](./imgs/2021-12-14_113640.png)
提醒无法连接到Microsoft.EntityFrameworkCore.Design所以在XXX.csproj文件中写以下代码既可，写完要dotnet restore
```C#
  <ItemGroup>
    <PackageReference Include="Microsoft.EntityFrameworkCore.Design" Version="3.1" />
  </ItemGroup>
```

接下来就可以生成数据库表



如图文件名Migrations

![无法加载](./imgs/2021-12-13_212200.png)


由于前面的一些不当操作导致生成的数据库表没有字段

![无法加载](./imgs/2021-12-13_212329.png)

正常

![无法加载](./imgs/2021-12-13_212506.png)

## 最后一步

生成的迁移文件，与数据库表信息同步

## 服务报错
![无法加载](./imgs/2021-12-14_113607.png)

启动sql服务可以解决。
## 主键报错
实体类中含Id，会自动识别为[Key]，[Key]可省略，若表没有Id则需要设置，不然会报错，如下：
![无法加载](./imgs/2021-12-14_113619.png)
意思是Models文件夹中的自定义模型要有Id属性(一定要写成Id，而不是ID，id，iD,不然创建表过程中无法识别主键)

如下图：

![无法加载](./imgs/2021-12-14_132500.png)

### 连接数据库代码
``` c#
dotnet ef database update
```

数据库中就有你模型表中的字段和数据库了。
